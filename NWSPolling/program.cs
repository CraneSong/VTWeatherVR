﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

// This approach was based off of http://www.dotnetperls.com/scraping-html
// The above way lets me pull all of the href elements of the web page via html scraping.
// NOTICE: should the html layout of the given url change too much then this approach will not work.


namespace NWSPolling
{
    class Program
    {
        static void Main()
        {
            
            // 1.
            //Main URL to notice : http://warnings.cod.edu/
            WebClient w = new WebClient();
            string s = w.DownloadString("http://warnings.cod.edu/");

            // 2.
            foreach (LinkItem i in LinkFinder.Find(s))
            {
                //To debug the output so that I can see that the hrefs have been pulled
                Debug.WriteLine(i);
            }
        }
    }
}
