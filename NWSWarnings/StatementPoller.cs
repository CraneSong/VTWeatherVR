﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Diagnostics;
using System.Text;

namespace NWSPolling
{
    // This is the main class to use WebClient to grab all the files from our clients database.
    // The website is http://warnings.cod.edu/
    // This website must be hit every 5 mins but the manager will control this sub project.
    public class StatementPoller
    {
        private List<LinkItem> downloadlist;
        private String websiteURL;
        private WebClient poller;
        private String fileContents; //content of a single file in string form
        private String[] webcontents = new String[500];//array of all the website information in string form

        WebClient client = new WebClient();

        /// <summary>
        /// Empty constructor to make a poller
        /// </summary>
        public StatementPoller()
        {
            poller = new WebClient();
        }

        /// <summary>
        /// Constructor for website polling
        /// </summary>
        /// <param name="websiteToPoll">The website to poll</param>
        public StatementPoller(String websiteToPoll)
        {
            this.websiteURL = websiteToPoll;
        }

        /// <summary>
        /// To poll the website for all of the href links do this 1st
        /// </summary>
        /// <param name="website"> the website to poll</param>
        public void pollWebsiteForLinks(String website)
        {

            WebClient w = new WebClient();
            string s = w.DownloadString(website);


            //now polling class has a copy of all the href element links for downloading
            downloadlist = LinkFinder.Find(s);
        }



        /// <summary>
        /// Downloads all the files from the base website and puts them into string form into an array
        /// Note takes about 1-2 minutes to fully download the website each time...
        /// </summary>
        public void DownloadAllFiles()
        {
            
            int count = 0;
            foreach (LinkItem i in downloadlist)
            {

                // Only catch here is the file is still reversed need to parse it again since previous way was not fully correct.
                if (i.Href == "2016042921.SVR"|| i.Href == "2016043006.FFW")
                {
                    Console.WriteLine("Caught that weird file...shouldn't be included now...");
                }
                else
                {

                    webcontents[count] = client.DownloadString("http://warnings.cod.edu/" + i.Href);

                    count++;
                }
            }

        }



        /// <summary>
        /// Takes param nameOfFile then polls the url and returns all information into string
        /// </summary>
        /// <param name="nameOfFile">needs to include href tag if coming from the website</param>
        /// <returns></returns>
        public String getFileAsString(String nameOfFile)
        {
            this.fileContents = poller.DownloadString(nameOfFile);

            return fileContents;   
        }

        /// <summary>
        /// To Return the file that the poller is currently looking at
        /// </summary>
        /// <returns>The file as a string should it exist</returns>
        public String getCurrentFileContents()
        {
            if (this.fileContents == null)
            {
                return "Sorry no file currently being looked at";
            }
            else
            {
                return this.fileContents;
            }
        }

        
        /// <summary>
        ///  grab the list of all files downloaded.
        /// </summary>
        /// <returns></returns>
        public List<LinkItem> getDownloadList()
        {
            return this.downloadlist;
        }

        //setter for website
        public void setWebsiteToPoll(String websiteToPoll)
        {
            this.websiteURL = websiteToPoll;
        }
        
        /// <summary>
        /// Getter for website URL
        /// </summary>
        /// <returns>String of website URL</returns>
        public String getWebsiteURL()
        {
            return this.websiteURL;
        }

        /// <summary>
        /// Setter for the website URL
        /// </summary>
        /// <param name="newWebsite">The new website URL</param>
        public void setWebsiteURL(String newWebsite)
        {
            this.websiteURL = newWebsite;
        }

        /// <summary>
        /// Returns the array of all files from the base website
        /// </summary>
        /// <returns>Returns the String array of all files of a website</returns>
        public String[] getWebsiteContents()
        {

            return this.webcontents;
        }
    }
}
