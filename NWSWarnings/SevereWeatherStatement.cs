﻿using System;
using System.Collections.Generic;
using System.Drawing;

// ========================================================================
//
//                          SevereWeatherStatement.cs
//
// Author: Sarah Nimitz                         Date: May 2, 2016
// Purpose: Represents a severe weather statement published by the National
//          Weather Service. It will include the information that it is
//          assigning to the relevant warning such as warning type, station
//          issuing the warning, event tracking number (ETN), expiration
//          time of the warning, and the vertices of the warning geometry.
//          It also contains information regarding the publication time of
//          the statement, the text of the statement, and the warning object
//          being referenced. In the case of statements being unattached to
//          warnings, of an unsupported type, or generally unparsable, they
//          are assigned default values to the missing data.
// 
// ========================================================================
namespace NWSWarnings
{

    public class SevereWeatherStatement
    {
        // ================================================================
        // Creates a SevereWeatherStatement object with an associated
        // warning object for future reference.
        //
        // @params initWarning - Warning associated with the SWS object
        // @params initText - Full text of the severe weather statement being
        //                      constructed into the SWS object
        // @params initPublishTime - DateTime object representing the time
        //                              of the statement's publication.
        // ================================================================
        public SevereWeatherStatement(Warning initWarning, string initText, DateTime initPublishTime)
        {
            warning = initWarning;
            text = initText;
            publishTime = initPublishTime;
            station = warning.Station;
            trackingNumber = warning.TrackingNumber;
            type = warning.Type;
            time = warning.Time;
            movement = warning.Movement;
            vertices = warning.Vertices;
        }

        // ================================================================
        // Creates a SevereWeatherStatement object without an associated
        // warning object.
        //
        // @params initText - Full text of the severe weather statement being
        //                      constructed into the SWS object
        // @params initPublishTime - DateTime object representing the time
        //                              of the statement's publication.
        // @params initStation - The 4-character code representing the 
        //                          weather station responsible for this
        //                          statement's publication.
        // ================================================================
        public SevereWeatherStatement(string initText, DateTime initPublishTime, string initStation)
        {
            warning = null;
            text = initText;
            publishTime = initPublishTime;
            station = initStation;
            trackingNumber = -1;
            type = WarningType.Unknown;
            time = DateTime.MinValue;
            movement = null;
            vertices = null;
        }

        // ================================================================
        // The Warning object that is being referenced by this SWS object.
        // warning is null if the SWS object does not reference a warning.
        // ================================================================
        private Warning warning;
        public Warning Warning
        {
            get { return this.warning; }
        }

        // ================================================================
        // A DateTime object representing the initial publication time of
        // the severe weather statement.
        // ================================================================
        private DateTime publishTime;
        public DateTime PublishTime
        {
            get { return this.publishTime; }
        }
        
        // ================================================================
        // The 4-character string representing the weather station that has
        // published the severe weather statement.
        // ================================================================
        private string station;
        public string Station
        {
            get { return this.station; }
            private set { station = value; }
        }

        // ================================================================
        // An integer representing the 4-digit event tracking number (ETN)
        // of the associated warning.
        // ================================================================
        private int trackingNumber;
        public int TrackingNumber
        {
            get { return this.trackingNumber; }
            private set { trackingNumber = value; }
        }

        // ================================================================
        // The WarningType enum that represents the type of warning that is
        // assigned the warning by the severe weather statement.
        // ================================================================
        private WarningType type;
        public WarningType Type
        {
            get { return this.type; }
            private set { type = value; }
        }

        // ================================================================
        // A DateTime object representing expiration time of the associated
        // warning, as assigned by the severe weather statement. 
        // ================================================================
        private DateTime time;
        public DateTime Time
        {
            get { return this.time; }
            private set { time = value; }
        }

        // ================================================================
        // An array of strings that represents the movements of an associated
        // warning's system as assigned by the severe weather statement.
        // This is in the format:
        //      * Time in Zulu the system was last observed
        //      * Degree (from North) of movement
        //      * Speed (in knots) the system is moving
        //      * Latitude of location
        //      * Longitude of location
        //      * [Any additional Lat/Lon pairs]
        // ================================================================
        private string[] movement;
        public string[] Movement
        {
            get { return this.movement; }
            private set { movement = value; }
        }

        // ================================================================
        // A list of PointF objects that represents the vertices parsed from
        // the severe weather statement to describe the geometry of the
        // associated warning as assigned by the statement.
        // ================================================================
        private List<PointF> vertices;
        public List<PointF> Vertices
        {
            get { return this.vertices; }
            private set { vertices = value; }
        }

        // ================================================================
        // A string that represents the full text of the severe weather
        // statement that is represented by this SWS object.
        // ================================================================
        private string text;
        public string Text
        {
            get { return this.text; }
            set
            {
                if (value == null)
                {
                    Console.Write("SevereWeatherStatement object has been assigned null text. Making empty.");
                    text = "";
                }
                else
                    text = value;
            }
        }
    }
}
