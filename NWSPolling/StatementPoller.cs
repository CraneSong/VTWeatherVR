﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Diagnostics;

namespace NWSPolling
{
    // This is the main class to use WebClient to grab all the files from our clients database.
    // The website is http://warnings.cod.edu/
    // This website must be hit every 5 mins but the manager will control this sub project.
    public class StatementPoller
    {
        public List<LinkItem> downloadlist;
        public String website;

        //basic constructor accepts a string for the website 
        public StatementPoller(String websiteToPoll)
        {
            this.website = websiteToPoll;
        }

        // To create a statement Poller for the given website
        public List<LinkItem> pollWebsiteForLinks(String website)
        {

            WebClient w = new WebClient();
            string s = w.DownloadString(website);

            // 2.
            foreach (LinkItem i in LinkFinder.Find(s))
            {

                downloadlist.Add(i);
                //To debug the output so that I can see that the hrefs have been pulled
                Debug.WriteLine(i);
            }

            //should now have a list of links in downloadlist
            return downloadlist;
        }



        // The param is a generic list of all the link items ie the href links
        public void DownloadFiles(List<LinkItem> list)
        {

            foreach (LinkItem i in list)
            {
                // Only catch here is the file is still reversed need to parse it again since previous way was not fully correct.
                WebClient client = new WebClient();
                client.DownloadFile("http://warnings.cod.edu/" + i.Href, i.Href + ".txt");
            }

        }


        // A test function that downloads a single file and labels it test1.txt
        public void DownloadFile()
        {
            WebClient client = new WebClient();
            client.DownloadFile("http://warnings.cod.edu/2016042121.SVS", "test1.txt");

        }



    }
}
