﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWSWarnings;
using NWSPolling;
using System.Text.RegularExpressions;

namespace TestPolling
{
    public class pollTest
    {
        
        static void Main()
        {
            StatementPoller poll = new StatementPoller();

            //a test of grabbing a single file via website URL as a string
            Console.WriteLine("Polling server for file\n");
            poll.getFileAsString("http://warnings.cod.edu/2016042912.SVR");

            Console.WriteLine("Here is the file as one latest statement is the last one");

            Console.WriteLine(poll.getCurrentFileContents());

            Console.WriteLine("Here is the file split via the regex to test if it works correctly");

            String[] sections = Regex.Split(poll.getCurrentFileContents(), "\n\n\n");

            for(int i =0; i < sections.Length;i++)
            {
                Console.WriteLine(sections[i]);
                Console.WriteLine("========================\n");
            }

     

            //now testing the web crawler for all the links
            Console.WriteLine("Going to try and see if grabbing all the files from the website works");
            Console.WriteLine("Might take a minute to warn...");

            poll.pollWebsiteForLinks("http://warnings.cod.edu/");
            poll.DownloadAllFiles();

            Console.WriteLine("Success all files have been pulled and pollers websiteContents has been updated correctly\n");

            Console.WriteLine("Test Ended and no problems!");

            Console.Read();

        }
    }
}
