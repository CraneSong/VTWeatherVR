﻿using NWSWarnings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TestWarningManager
{
    class managerTest
    {
        private static int intervalPoll = 300000; //5 mins in milliseconds
        private static String PollingURL = "http://warnings.cod.edu/";
        private static WarningManager tester;

        public static void Main()
        {

            Console.WriteLine("About to test the WarningsManager Poll() function");
            tester = new WarningManager(PollingURL, intervalPoll);

            tester.Poll();//currently runs once then all done...

            Console.WriteLine("Test of polling done!");
            Console.Read();

            Console.WriteLine(tester.GetWarnings().ToString());


            /* currently an infinite loop since process in not set as a
            Console.WriteLine("About to enter polling then immediately going to call to abort!");
            tester.Poll();
            Console.WriteLine("Aborting the polling now!");
            tester.StopPolling();
            Console.WriteLine("Test of polling done!");

            */
        }
    }
}
